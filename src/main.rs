use sysinfo::{System, SystemExt};

fn main() {
    let mut sys: System = System::new_all();

    sys.refresh_all();

    // RAM and swap information:
    println!("total memory: {} KB", sys.total_memory());
    println!("used memory : {} KB", sys.used_memory());

    // Display system information:
    println!("System name:             {:?}", sys.name());
    println!("System kernel version:   {:?}", sys.kernel_version());
    println!("System OS version:       {:?}", sys.os_version());
    println!("System host name:        {:?}", sys.host_name());

    // Number of processors:
    println!("processors: {}", sys.processors().len());
}
